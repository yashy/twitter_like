#This code takes 2 arguments- user and message and adds it to the 
#table(using DBtable.py) and displays all the contents of the table.
#Author: YASH YADAV


#!/usr/bin/python

import sys

from boto.dynamodb2.fields import HashKey, RangeKey, AllIndex
from boto.dynamodb2.table import Table
from boto.dynamodb2.items import Item
from boto.dynamodb2.layer1 import DynamoDBConnection
#import our class DBtable
import DBtable    

user = sys.argv[1]
message = sys.argv[2]
#print "User:%s"%user
#print "Message:%s"%message


messages = DBtable.Init()

"""
Fill in the initial employee data. The "overwrite" parameter is True so it will overwrite
any existing employee data.
"""
data = {'id' : message, 'message':message, 'user':user}
messages.put_item(data=data,overwrite=True)


# Use scan to dump all employees
print "Messages:"
for emp in messages.scan():
    print
    DBtable.PrintItem(emp)


