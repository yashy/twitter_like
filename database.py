#Author: Yash Yadav


import boto
from boto.dynamodb2.fields import HashKey, RangeKey, AllIndex
from boto.dynamodb2.table import Table
from boto.dynamodb2.items import Item
from boto.dynamodb2.layer1 import DynamoDBConnection
from boto.dynamodb2.types import NUMBER
from pprint import *
import inspect
import ConfigParser
import time

def Init():
    """
    Connect to DynamoDB Local. If you want to connect to the real DynamoDB set the 'local'
    variable below to Fals, but make sure either you have a .boto file or you
    pass both the aws_access_key_id and aws_secret_access_key parameters to the create
    (this code fetches them from settings.cfg).
    """
    local = False
    #print "Inside class"
    if local:
        # Connect to local DynamoDB server. Make sure you have that running first.
        conn = DynamoDBConnection(
            host='localhost',
            port=8001,
            aws_secret_access_key='foo',
            aws_access_key_id='foo',
            is_secure=False)
    else:
        # Connect to the real DynamoDB.
        config = ConfigParser.RawConfigParser()
        config.read("settings.cfg")
        idd = config.get('DynamoDB', 'aws_access_key_id')
        key = config.get('DynamoDB', 'aws_secret_access_key')

        conn = boto.dynamodb2.connect_to_region('us-west-2',
            aws_access_key_id = idd, aws_secret_access_key = key)

    #print "connection successful"
    # Get a list of all tables from DynamoDB.
    tables = conn.list_tables()    

    #print "Table created"

    if 'TEST94' not in tables['TableNames']:
        #print "Inside If"
        # Create table of employees
        #print "Creating new table"
        employees = Table.create('TEST94',
                                 schema = [HashKey('Type'), RangeKey('UniqueTime')],   #defaults to String data type 
                                 indexes = [AllIndex('UserIndex', parts = [
                                                HashKey('Type'),RangeKey('id')]),
                                            AllIndex('TweetIndex',parts = [
                                                HashKey('Type'),RangeKey('Key')])],
                                 connection = conn)
        # Wait for table to be created (DynamoDB has eventual consistency)
        while True:
            time.sleep(5)
            try:
                conn.describe_table('TEST94')
            except Exception, e:
                print e
            else:
                break            

    else:
        #print "Inside else"
        # Use the existing table.
        employees = Table('TEST94', connection=conn)   

    return employees    


def PrintItem(item):
    for (field, val) in item.items():
        print "%s: %s" % (field, val)
